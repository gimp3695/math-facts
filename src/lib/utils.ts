type MathOperation = 'addition' | 'subtraction';
export interface MathProblem {
	problem: { numerator: number; denominator: number; operation: MathOperation };
	answer: number;
}

export function generateMathQuestions(fact: number, questionCount: number, operation: MathOperation): MathProblem[] {
	const problems = [];
	for (let i = 0; i < questionCount; i++) {
		const swapNumeratorAndDenominator = Math.random() > 0.5;
		let numerator: number = 0;
		let denominator: number = 0;
		if (swapNumeratorAndDenominator) {
			numerator = fact;
			denominator = Math.floor(Math.random() * 10);
		} else {
			numerator = Math.floor(Math.random() * 10);
			denominator = fact;
		}

		if (operation === 'subtraction' && numerator < denominator) {
			// swap
			const temp = numerator;
			numerator = denominator;
			denominator = temp;
		}

		const answer = operation === 'addition' ? numerator + denominator : numerator - denominator;
		problems.push({
			problem: {
				numerator,
				denominator,
				operation
			},

			answer
		});
	}
	return problems;
}
