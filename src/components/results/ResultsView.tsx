'use client';
import * as React from 'react';
import './ResultsView.scss';
import { Box } from '@redskytech/framework/ui';

import { recordedAnswers } from '@/components/test/mathTestContent/MathTestContent';
import { problems } from '@/components/test/mathTestContent/MathTestContent';
import { Label } from '@redskytech/framework/ui/label/Label';
import { Check, X } from 'lucide-react';
import themes from '@/themes/themes.module.scss';

interface ResultsViewProps {}

const ResultsView: React.FC<ResultsViewProps> = (props) => {
	const correctAnswers = problems.filter((problem) => {
		return problem.answer === recordedAnswers[problems.indexOf(problem)];
	});

	return (
		<Box className={'rsResultsView'}>
			<Label variant={'h3'} weight={'regular'} textAlign={'center'} mb={16}>
				You got {correctAnswers.length} out of {problems.length} correct!
			</Label>
			{recordedAnswers.map((answer, index) => {
				const problem = problems[index];
				return (
					<Box key={index} display={'flex'} mb={8} gap={8}>
						<Label variant={'h5'} weight={'regular'}>
							{`${problem.problem.numerator} ${problem.problem.operation === 'addition' ? '+' : '-'} ${
								problem.problem.denominator
							}`}{' '}
							= {answer}
						</Label>
						{problem.answer === answer ? (
							<Check color={themes.accentSuccess} />
						) : (
							<>
								<X color={themes.accentError} />
								<Label variant={'h5'} weight={'regular'}>
									correct = {problem.answer}
								</Label>
							</>
						)}
					</Box>
				);
			})}
		</Box>
	);
};

export default ResultsView;
