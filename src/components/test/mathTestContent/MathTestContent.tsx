'use client';
import * as React from 'react';
import './MathTestContent.scss';
import { Box, Button, Label } from '@redskytech/framework/ui';
import { useRouter, useSearchParams } from 'next/navigation';
import { useEffect, useState } from 'react';
import { generateMathQuestions, MathProblem } from '@/lib/utils';
import { Plus, Minus } from 'lucide-react';
import themes from '@/themes/themes.module.scss';

const QUESTION_COUNT = 20;
const TEST_DURATION = 60;

export let recordedAnswers: number[] = [];
export let problems: MathProblem[] = [];

interface MathTestContentProps {}

const MathTestContent: React.FC<MathTestContentProps> = (props) => {
	const test = useSearchParams();
	const fact = Number(test.get('facts') || '1');
	const operation = (test.get('operation') || 'addition') as 'addition' | 'subtraction';

	const [readyCountDown, setReadyCountDown] = useState<number>(3);
	const [isInReadyMode, setIsInReadyMode] = useState<boolean>(false);

	const [timeRemainingSecs, setTimeRemainingSecs] = useState<number>(TEST_DURATION);
	const [currentProblemIndex, setCurrentProblemIndex] = useState<number>(0);

	const [currentAnswer, setCurrentAnswer] = useState<number | undefined>();

	const router = useRouter();
	router.prefetch('/results');

	useEffect(() => {
		if (readyCountDown >= 0 || problems.length > 0) return;
		problems = generateMathQuestions(fact, QUESTION_COUNT, operation);
		setIsInReadyMode(false);
	}, [readyCountDown]);

	useEffect(() => {
		recordedAnswers = [];
		problems = [];
		setIsInReadyMode(true);
		window.sessionStorage.setItem('fact', fact.toString());
		window.sessionStorage.setItem('operation', operation);
	}, []);

	useEffect(() => {
		const timeoutId = setTimeout(() => {
			if (readyCountDown >= 0) {
				setReadyCountDown(readyCountDown - 1);
			} else {
				if (timeRemainingSecs === 0) {
					router.push('/results');
				} else {
					setTimeRemainingSecs(timeRemainingSecs - 1);
				}
			}
		}, 1000);
		return () => clearTimeout(timeoutId);
	}, [readyCountDown, timeRemainingSecs]);

	function renderCurrentProblem(): React.ReactNode {
		if (!problems.length) return <></>;
		const currentProblem = problems[currentProblemIndex];
		return (
			<Box className={'currentProblem'}>
				<Label variant={'display1'} weight={'semiBold'}>
					{currentProblem.problem.numerator}
				</Label>
				<Box display={'flex'} alignItems={'center'} gap={16}>
					{operation === 'addition' ? <Plus size={40} /> : <Minus size={40} />}
					<Label variant={'display1'} weight={'semiBold'}>
						{currentProblem.problem.denominator}
					</Label>
				</Box>
				<Box width={'100%'} height={4} bgColor={themes.neutralPurple300} />
				<Box display={'flex'} alignItems={'center'} gap={16}>
					<Label variant={'display1'} weight={'semiBold'} className={'answerValue'}>
						{currentAnswer === undefined ? '' : currentAnswer}
					</Label>
				</Box>
			</Box>
		);
	}

	function handleInputNumber(value: number) {
		let currentAnswerStr = (currentAnswer ?? '').toString();
		currentAnswerStr += value.toString();
		setCurrentAnswer(Number(currentAnswerStr));
	}

	function handleSubmit() {
		if (currentAnswer === undefined) return;
		recordedAnswers.push(currentAnswer);
		setCurrentAnswer(undefined);

		if (currentProblemIndex + 1 === QUESTION_COUNT) {
			router.push('/results');
		} else {
			setCurrentProblemIndex(currentProblemIndex + 1);
		}
	}

	function handleDelete() {
		if (!currentAnswer) return;
		let currentAnswerStr = currentAnswer.toString();
		currentAnswerStr = currentAnswerStr.slice(0, -1);
		setCurrentAnswer(!currentAnswerStr ? undefined : Number(currentAnswerStr));
	}

	function renderInput() {
		if (!problems.length) return <></>;
		return (
			<Box className={'inputGrid'}>
				{[1, 2, 3, 4, 5, 6, 7, 8, 9, 0].map((item) => {
					return (
						<Button look={'containedSecondary'} key={item} onClick={() => handleInputNumber(item)}>
							{item}
						</Button>
					);
				})}
				<Button look={'containedSecondary'} onClick={handleDelete}>
					Del
				</Button>
				<Button look={'containedPrimary'} onClick={handleSubmit}>
					Submit
				</Button>
			</Box>
		);
	}

	function renderGetReady() {
		if (problems.length) return <></>;
		return (
			<Label variant={'display1'} weight={'regular'}>
				Ready in {readyCountDown}
			</Label>
		);
	}

	return (
		<Box className={'rsMathTestContent'}>
			<Label variant={'h1'} weight={'regular'} className={'operationHeader'}>
				{operation}
			</Label>
			<Box display={'flex'} justifyContent={'space-between'}>
				<Label variant={'h4'} weight={'regular'}>
					Question {currentProblemIndex + 1}/{QUESTION_COUNT}
				</Label>
				<Label
					variant={'h4'}
					weight={'regular'}
					color={timeRemainingSecs < 10 ? themes.accentError : themes.neutralPurple100}
				>
					Time Left: {timeRemainingSecs}
				</Label>
			</Box>
			{isInReadyMode && renderGetReady()}
			{!isInReadyMode && renderCurrentProblem()}
			{!isInReadyMode && renderInput()}
		</Box>
	);
};

export default MathTestContent;
