'use client';
import * as React from 'react';
import './FactGrid.scss';
import { Box, Button } from '@redskytech/framework/ui';
import Link from 'next/link';

interface FactGridProps {
	operation: 'addition' | 'subtraction';
}

const FactGrid: React.FC<FactGridProps> = (props) => {
	const buttonTypes: { [key: string]: number } = {
		'1s': 1,
		'2s': 2,
		'3s': 3,
		'4s': 4,
		'5s': 5,
		'6s': 6,
		'7s': 7,
		'8s': 8,
		'9s': 9
	};

	return (
		<Box className={'rsFactGrid'}>
			{Object.keys(buttonTypes).map((key) => (
				<Link
					href={{ pathname: 'test', query: { operation: props.operation, facts: buttonTypes[key] } }}
					key={key}
				>
					<Button look={'containedPrimary'} fullWidth>{key}</Button>
				</Link>
			))}
		</Box>
	);
};

export default FactGrid;
