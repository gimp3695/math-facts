'use client';
import * as React from 'react';
import './Counter.scss';
import { Box, Button } from '@redskytech/framework/ui';
import { useEffect, useState } from 'react';
import { Label } from '@redskytech/framework/ui';

interface CounterProps {
	startingCount: number;
}

const Counter: React.FC<CounterProps> = (props) => {
	const [count, setCount] = useState<number>(props.startingCount);

	useEffect(() => {
		console.log('Joshua: Counter -> count', count);
		return () => console.log('clean up');
	}, [count]);

	useEffect(() => {
		setCount(15);
	}, []);

	return (
		<Box className={'rsCounter'}>
			<Label variant={'h3'} weight={'regular'}>
				{count}
			</Label>
			<Button look={'containedPrimary'} onClick={() => setCount(count + 1)}>
				Increment
			</Button>
		</Box>
	);
};

export default Counter;
