'use client';
import * as React from 'react';
import { Box, Button } from '@redskytech/framework/ui';
import Link from 'next/link';

interface MathTypeSelectionProps {}

const MathTypeSelection: React.FC<MathTypeSelectionProps> = (props) => {
	return (
		<Box className={'rsMathTypeSelection'}>
			<Box mb={24} mt={16}>
				<Link href={'/addition/facts'}>
					<Button
						look={'containedPrimary'}
						fullWidth
					>
						Addition
					</Button>
				</Link>
			</Box>
			<Box>
				<Link href={'/subtraction/facts'}>
					<Button
						look={'containedSecondary'}
						fullWidth
					>
						Subtraction
					</Button>
				</Link>
			</Box>
		</Box>
	);
};

export default MathTypeSelection;
