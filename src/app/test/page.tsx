import MathTestContent from '@/components/test/mathTestContent/MathTestContent';

export default function MathTest() {
	return (
		<main className={'rsMathTestPage'}>
			<MathTestContent />
		</main>
	)
};

