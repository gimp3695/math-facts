import { Space_Mono } from '@next/font/google';

export const spaceMono = Space_Mono({
	weight: ['400', '700'],
	variable: '--font-space-mono',
	subsets: ['latin'],
	display: 'optional'
})