import { Label } from '@redskytech/framework/ui/label/Label';
import MathTypeSelection from '@/app/MathTypeSelection';
import './page.scss';
import { Box } from '@redskytech/framework/ui/box/Box';

export default async function HomePage() {
	return (
		<main className={'rsHomePage'}>
			<Label variant={'h1'} weight={'regular'} textAlign={'center'} >
				Math Facts
			</Label>
			<Box width={'100%'} height={2} bgColor={'orange'}/>
			<MathTypeSelection />
		</main>
	);
}
