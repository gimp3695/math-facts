'use client';
import Link from 'next/link';
import { Home } from 'lucide-react';
import themes from '../../../themes/themes.module.scss';
import { Box } from '@redskytech/framework/ui/box/Box';

export default function DashboardLayout({ children }: { children: React.ReactNode }) {
	return (
		<section>
			<Box className={'headerBar'}>
				<Link href={'/'}>
					<Home color={themes.neutralPurple100} />
				</Link>
			</Box>
			{children}
		</section>
	);
}
