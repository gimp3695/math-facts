import DefaultHead from '@/app/DefaultHead';

export default function Head() {
	return (
		<>
			<DefaultHead />
			<title>Math Facts - Selection</title>
			<meta name="description" content="Selection for facts" />
		</>
	);
}
