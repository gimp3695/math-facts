import { Label } from '@redskytech/framework/ui/label/Label';
import './page.scss';
import FactGrid from '@/components/facts/factGrid/FactGrid';
import themes from '@/themes/themes.module.scss'

export default async function FactsPage({ params }: { params: { type: 'addition' | 'subtraction' } }) {
	return (
		<main className={'rsFactsPage'}>
			<Label variant={'h1'} weight={'semiBold'} textTransform={'capitalize'} textAlign={'center'}>
				{params.type}
			</Label>
			<Label variant={'body1'} weight={'regular'} color={themes.primaryOrange100}>Choose a number to work on.</Label>
			<FactGrid operation={params.type} />
		</main>
	);
}
