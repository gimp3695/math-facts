import DefaultHead from '@/app/DefaultHead';

export default function Head() {
	return (
		<>
			<DefaultHead />
			<title>Math Facts</title>
			<meta name="description" content="A math facts website" />
		</>
	);
}
