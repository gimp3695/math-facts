'use client';
import { Label } from '@redskytech/framework/ui/label/Label';
import ResultsView from '@/components/results/ResultsView';
import { Box } from '@redskytech/framework/ui/box/Box';
import { Button } from '@redskytech/framework/ui/button/Button';
import themes from '@/themes/themes.module.scss';
import './page.scss';
import Link from 'next/link';

export default function ResultsPage() {
	let link = '';
	if (typeof window !== 'undefined')
		link = `/test?operation=${window.sessionStorage.getItem('operation')}&facts=${sessionStorage.getItem('fact')}`;

	return (
		<main className={'rsResultsPage'}>
			<Label variant={'h2'} weight={'regular'} mb={4}>
				Results
			</Label>
			<Box width={'100%'} height={2} bgColor={themes.primaryOrange100} mb={16} />
			<ResultsView />
			<Link href={link}>
				<Button mt={64} mb={24} look={'containedPrimary'} fullWidth>
					Try Again
				</Button>
			</Link>
			<Link href={'/'}>
				<Button look={'containedSecondary'} fullWidth>
					Go Home
				</Button>
			</Link>
		</main>
	);
}
