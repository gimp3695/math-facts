declare module '*.module.scss' {
	const value: {
		neutralWhite: string;
		neutralPurple100: string;
		neutralPurple200: string;
		neutralPurple300: string;
		neutralPurple400: string;
		neutralPurple500: string;
		neutralBlack: string;


		// Primary colors
		primaryOrange100: string;

		success: string;
		accentError: string;
		accentErrorDark: string;
		accentWarning: string;
		accentWarningLight: string;
		accentWarningDark: string;
		accentInfo: string;
		accentInfoLight: string;
		accentSuccess: string;
		accentSuccessDark: string;

		budgetDev: string;
		budgetBugs: string;
		budgetPM: string;
		budgetMeetings: string;
		budgetAdmin: string;
		budgetDesign: string;
		budgetOther: string;
		budgetQA: string;

		// Gradients
		gradient90Deg: string;
		gradient90Deg_20: string;
	};
	export = value;
}
